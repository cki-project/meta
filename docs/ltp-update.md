# LTP update procedure

Instead of using [upstream's LTP] directly, we have our own copy of
LTP for a commit we trust. This is to avoid that breakage in LTP's
`master` branch can make our test suites less stable. But this also
means that we need to periodically update this commit so we can pull
in recent fixes and updated tests from LTP repository.

To update our copy of LTP, we internally follow these steps:

* Find the most recent Jira issue with the title
  "[Update LTP to a recent commit]" and clone it ("More ⌄" 🠖 "Clone").
* Make your own branch of [kernel-tests] (_create a fork if you didn't
  have one already_).
* Look for a recent commit in the [upstream LTP repo] **that passes
  all Travis checks** and update the commit hash in the `git checkout`
  command in [runtest.sh] (function `ltp_test_build`) to use that
  commit.
* If there are any known issues that are reportedly now fixed
  (eg. tests that now can run because they have been improved, or
  because kernel bugs have been fixed), drop them from
  [knownissue.sh].
* Push the branch with your changes.
* Look for the Beaker job in the description of the Jira issue and
  click on it.
* Clone the Beaker job, but updating the branch used for LTP so that
  it uses the branch you just pushed (look for the `url` property in
  the `<fetch ...>` element inside the `<task name="LTP lite" ...>`
  element).
* Update the Beaker job link in the Jira issue to point to the Beaker
  job you just created.

[upstream's LTP]: https://github.com/linux-test-project/ltp
[Update LTP to a recent commit]: https://projects.engineering.redhat.com/browse/FASTMOVING-1437?jql=text%20~%20%22update%20ltp%20to%20a%20recent%20commit%22
[kernel-tests]: https://gitlab.com/cki-project/kernel-tests
[upstream LTP repo]: https://github.com/linux-test-project/ltp/commits/master
[runtest.sh]: https://gitlab.com/cki-project/kernel-tests/-/blob/master/distribution/ltp-upstream/lite/runtest.sh
[knownissue.sh]: https://gitlab.com/cki-project/kernel-tests/-/blob/master/distribution/ltp-upstream/include/knownissue.sh
