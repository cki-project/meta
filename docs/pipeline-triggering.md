# Pipeline triggering

## Regular ways to trigger pipelines

Pipelines can be triggered in multiple ways depending on what's being tested.
Most traditional ways are handled via the **[pipeline-trigger] repository**.
This repository contains code to trigger pipelines for:

* Plain git repositories, after every push to the branch(es)
* Email patch series, via queries to a [Patchwork] instance
* Already built kernels from Fedora build systems (Koji/COPR)

Patch and git triggers are supposed to run as cron jobs, while the build system
trigger needs to be constantly deployed as it acts as a listener on message buses.

### Kernels living in GitLab.com

With GitLab, one can use merge requests to propose changes to a kernel so there's
no need to have a cron to test the changes, as pipelines can automatically start
on every merge request submission and change. However, because we use our own
resources for testing, this method is only used for trusted contributors as
running unknown code in someone's infrastructure is a bad idea due to security
concerns.

Merge request testing is based on a [multiproject pipeline setup]. The kernel
repository is only used to trigger the pipeline in a different repository. The
reason for this is again that we're using our own resources, and these are
shared for all GitLab kernel projects we're testing. For maintenance, it's way
easier to add our own runners and pipeline setup into one project rather than
each individual kernel repo and its forks. All of the actual pipeline runs happen
in the [cki-runs] group.

Trusted contributors are members of the appropriate cki-runs project which allows
them to automatically start the pipelines there. The pipelines are started via
[bridge jobs] defined in `.gitlab-ci.yml` of the kernel repo. Example of such a
`.gitlab-ci.yml` definition:

```
workflow:
  rules:
    - if: '$CI_PIPELINE_SOURCE =~ /^(merge_request_event|web|api|trigger)$/'

trigger_pipeline:
  variables:
    # Insert any pipeline variables needed here
  trigger:
    project: cki-project/cki-runs/trusted-pipelines
    branch: kernel-ark
    strategy: depend
```

As this CI definition lives directly in the kernel repo, contributors have the
ability to adjust it if their kernel changes require it. However, **only trusted
contributors will see their changes reflected in the running CI pipeline**!

Contributors that don't have access to the cki-runs projects can still have their
changes tested, however they can't change the CI definition themselves. Testing
pipelines for external contributors are triggered via a [webhook]. This webhook
listens for any updates on the merge requests and creates pipelines based on a
[predefined configuration]. Two types of pipelines are possible - a *limited*
testing pipeline running isolated in AWS (`x86_64`-only) that will be automatically
started; and a *full* testing pipeline (all supported architectures) that requires
a review (and explicit button clicking) from a trusted contributor to run.

The configuration for external contributors currently lives in the internal
deployment-all repository, however it is planned to move it to publicly
available [pipeline-data] where the configuration for [pipeline-trigger] already
lives.

## Production and testing retriggers

Both production and testing pipelines can be created by retriggering an already
existing pipeline. existing pipeline. A production pipeline is only created when
the `IS_PRODUCTION` environment variable is set to a truthy value (`true/True`).
If that is not the case, a testing pipeline is created instead. Testing pipelines
have an extra `retrigger = true` variable added to ensure correct handling.

An already existing pipeline can be retriggered by the [retrigger script].

Pipelines for Fedora build systems can be retriggered or created by the
[brew trigger script]. The task ID is obtained via the CLI instead of the message
bus.

Only the [brew trigger script] accepts the regular config file from [pipeline-trigger];
the [retrigger script] takes all configuration from the original pipeline. However,
both scripts accept trigger variable overrides in case an adjustment is needed. This
allows creation of production pipelines that were not created e.g. because of
networking issues during pipeline YAML downloads - such pipelines don't have any jobs
or trigger variables that can be queried and thus a regular retrigger doesn't work
there.

Bot testing done by `cki-ci-bot` on various repositories uses the same code paths as
the [retrigger script] in the background.

### Retriggering examples

To retrigger a pipeline in GitLab, you need both a personal access token and a
project trigger token. The personal access token can be generated in your
[GitLab user settings] and needs to be saved in the `GITLAB_PRIVATE_TOKEN` environment
variable. The trigger token can be generated in the [project settings] of the project
where the pipeline is supposed to be triggered and needs to be saved in the
`GITLAB_TRIGGER_TOKEN` environment variable. The base URL of the GitLab instance
containing the pipeline project should be saved in the `GITLAB_URL` variable.
Optionally, the `IS_PRODUCTION` variable can be exposed.

Once you've done all of this, you can trigger a pipeline with the [retrigger script]
like

```bash
python3 -m pipeline_tools retrigger \
        --project cki-project/cki-pipeline \
        --pipeline-id PIPELINE_ID
```

The example will query the pipeline `PIPELINE_ID` from the `cki-project/cki-pipeline`
project, and create a new pipeline in the same project.

Extra variables and variable overrides can be provided via the `--variables`
option. In the following example, the Beaker testing is being skipped:

```bash
python3 -m pipeline_tools retrigger \
        --project cki-project/cki-pipeline \
        --pipeline-id PIPELINE_ID \
        --variables skip_beaker=false
```

You can trigger a pipeline with the [brew trigger script] like

```bash
python3 -m pipeline_tools.brew_trigger 1234567
```

In the above example, configuration is taken from the default local file `brew.yaml`.
This can be overriden with the `-c/--config <path>` or `--config-url <url>` options.

[pipeline-trigger]: https://gitlab.com/cki-project/pipeline-trigger/
[Patchwork]: https://github.com/getpatchwork/patchwork/
[multiproject pipeline setup]: https://docs.gitlab.com/ee/ci/multi_project_pipelines.html
[cki-runs]: https://gitlab.com/cki-project/cki-runs
[bridge jobs]: https://docs.gitlab.com/ee/ci/multi_project_pipelines.html#triggering-a-downstream-pipeline-using-a-bridge-job
[webhook]: https://gitlab.com/cki-project/kernel-webhooks/-/blob/main/webhook/external_ci.py
[predefined configuration]: https://gitlab.com/cki-project/kernel-webhooks#configuration-format-and-example
[pipeline-data]: https://gitlab.com/cki-project/pipeline-data/
[retrigger script]: https://gitlab.com/cki-project/cki-tools/-/blob/master/pipeline_tools/retrigger.py
[brew trigger script]: https://gitlab.com/cki-project/cki-tools/-/blob/master/pipeline_tools/brew_trigger.py
[GitLab user settings]: https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token
[project settings]: https://docs.gitlab.com/ee/ci/triggers/README.html#adding-a-new-trigger
