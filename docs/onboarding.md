# CKI Onboarding

## Enable Testing for your Kernel Tree

If you would like to enable testing for your kernel tree, we'll need to know the following specifics:

* Which kernel tree and branch we should test ?
* Which tests should be run ?
* Which arches should we test ?
* Where should we send results ?

Please file a [PR](https://gitlab.com/cki-project/pipeline-data) and we can help you fill in the remaining fields, or send email to [cki-project@redhat.com](mailto:cki-project@redhat.com) with the details above.

## Contributing Tests

There are various ways to contribute tests to CKI. LTP and kselftests are included test suites, among many others in CKI, for a full list of test suites visit our [public tests repo](https://gitlab.com/cki-project/kernel-tests).

### LTP

* LTP Test source lives at the [Linux Test Project](https://linux-test-project.github.io/)
* Contact: Send questions to the mailing list [ltp@lists.linux.it](mailto:ltp@lists.linux.it). For full [contact information](https://github.com/linux-test-project/ltp/wiki/Contact-Info), including IRC, see the [LTP wiki](https://github.com/linux-test-project/ltp/wiki/Developers)
* Proposing patches: You can submit Pull Requests to the [LTP project on GitHub](https://github.com/linux-test-project/ltp).
* Documentation: The [LTP wiki](https://github.com/linux-test-project/ltp/wiki/Developers) has plenty of documentation to get you started. You have a [step-by-step tutorial to create a new C test](https://github.com/linux-test-project/ltp/blob/master/doc/c-test-tutorial-simple.txt), [Guidelines](https://github.com/linux-test-project/ltp/blob/master/doc/test-writing-guidelines.txt), and other documentation.

### kselftests

* The kernel contains a set of “self tests” under the tools/testing/selftests/ directory
* See [Contributing to kselftests](https://www.kernel.org/doc/html/latest/dev-tools/kselftest.html#contributing-new-tests) for instructions to add tests
* You can find more details at the [kselftests wiki](https://kselftest.wiki.kernel.org)
* Contact: Send questions to the mailing list [linux-kselftest@vger.kernel.org](mailto:linux-kselftest@vger.kernel.org) or join #linux-kselftest IRC Channel on freenode

### Standalone Tests

* If you want to contribute a stand alone test to CKI, you have an [example](https://gitlab.com/cki-project/kernel-tests/tree/master/example) that can be used as a template.
* Please file a PR to to add a test to our [public tests repo](https://gitlab.com/cki-project/kernel-tests)
