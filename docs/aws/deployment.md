# CKI AWS Deployment

*This is a work in progress.*

The CKI infrastructure can be deployed on AWS.
The necessary tooling can be found in the [aws-deployment] repository on GitLab.

[aws-deployment]: https://gitlab.com/cki-project/aws-deployment/

## Preparations

Before running the playbooks, be sure you are fully authenticated with AWS.

You must setup the following environment variables before starting any of the scripts:

- `GITLAB_URL`: URL of the GitLab instance to host the schedules and pipelines
- `GITLAB_PARENT_PROJECT`: GitLab parent project
- `GITLAB_TOKEN`: GitLab API token for the bot user that triggers pipelines
- `GITLAB_TOKEN_ADMIN_BOT`: maintainer-level GitLab API token to register GitLab runners
- `GITLAB_TOKEN_REGISTRATION`: GitLab runner registration token
- `GITLAB_TRIGGER_TOKEN_CKI`: GitLab trigger token for CKI pipeline repository
- `GITLAB_COM_SSH_ACCOUNTS`: space-separated list of usernames on gitlab.com which ssh keys should be allowed for access to the GitLab runner

Verify that the `aws_region` is set properly in `vars.yml`.
The default region is `us-east-2` (Ohio).

Ensure Ansible and the `boto3` python module are installed.

## Deploying

Run the playbook with the following command:

```shell
ansible-playbook -i hosts PLAYBOOK_NAME
```

`PLAYBOOK_NAME` is one of the following:

- `deploy_iam.yml`: Deploy or update roles and policies
- `deploy_common_infrastructure.yml`: Deploy or update VPCs, S3 buckets, and EC2 security groups
- `deploy_gitlab_runner.yml`: Deploy or update the gitlab-runner
- `deploy_gitlab_schedules.yml`: Deploy or update the GitLab CI/CD schedules
- `deploy_tear_down.yml`: Remove all EC2 instances and common infrastructure

Normally, the deployment is scoped to the user name and the subnet defined in `vars.yml`.
For production, override the `cki_environment` and `cki_cidr_base` variables, e.g. with

```shell
ansible-playbook \
    deploy_gitlab_runner.yml \
    -i hosts \
    -e cki_cidr_base=10.10 \
    -e cki_environment=production
```

To apply a certain role to a host at a fixed IP address, you can use

```shell
ansible-playbook \
    deploy_gitlab_runner.yml \
    -i gitlab_runner, \
    -e ansible_host=1.2.3.4
```
